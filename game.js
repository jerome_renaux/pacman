/**
 * Created by Jerome on 30-03-16.
 */
/*
 * Author: Jerome Renaux
 * E-mail: jerome.renaux@gmail.com
 */

var Game = {};

Game.preload = function() {
    game.load.tilemap('map', 'assets/maps/pacman_tunnels.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.image('tileset', 'assets/pacman3.png');
    game.load.spritesheet('pacman','assets/pacsprites.png',32,32);
    game.load.spritesheet('barrier','assets/barrier.png',64,32);
    game.load.bitmapFont('gameover', 'assets/fonts/videogame.png', 'assets/fonts/videogame.fnt');
    game.scale.pageAlignHorizontally = true;
    Game.easystar = new EasyStar.js();
};

Game.create = function(){
    // Create the map from the JSON file
    Game.map = game.add.tilemap('map');
    Game.map.addTilesetImage('pacman3', 'tileset');
    Game.layer = Game.map.createLayer('collision');
    Game.layer.resizeWorld();
    //Game.layer.debug = true;
    Game.map.setCollisionBetween(0,5); // Tile IDs, not positions (see JSON file to know ID's)
    Game.map.setCollisionBetween(8,23); // ID = tiledID + 1

    // Create an arrzy of 1's and 0's to use for the pathfinding
    Game.collisionArray = [];
    for(var i = 0; i < Game.map.height; i++){
        var col = [];
        for(var j = 0; j < Game.map.width; j++) {
            // Check if the tile at positions i,j generates a collision or not
            col.push(+Game.map.getTile(j,i,Game.layer,true).collides); // "+" to convert boolean to int ; true to prevent getting null tiles
        }
        Game.collisionArray.push(col);
    }
    //console.log(Game.collisionArray.join("\n"));
    Game.easystar.setGrid(Game.collisionArray);
    Game.easystar.setAcceptableTiles([0]);
    //Game.easystar.setIterationsPerCalculation(1000);

    Game.pellets = game.add.group();
    Game.pellets.enableBody = true;
    Game.map.createFromTiles(6, -1, 'pacman', Game.layer, Game.pellets, {frame:54});
    Game.eatenPellets = 0;

    Game.superPellets = game.add.group();
    Game.superPellets.enableBody = true;
    Game.map.createFromTiles(7, -1, 'pacman', Game.layer, Game.superPellets, {frame:55});

    Game.pacman = game.add.sprite(14*32, 17*32, 'pacman',10);
    Game.pacman.speed = 120;
    Game.pacman.orientation = 'right';
    game.physics.arcade.enable(Game.pacman);
    Game.setAnimations(Game.pacman);
    Game.over = false;

    Game.ghosts = game.add.group();
    Game.ghosts.enableBody = true;
    Game.clyde = game.add.sprite(12*32,14*32,'pacman',2);
    Game.blinky = game.add.sprite(13*32,14*32,'pacman',0);
    Game.pinky = game.add.sprite(14*32,14*32,'pacman',4);
    Game.inky = game.add.sprite(15*32,14*32,'pacman',6);
    Game.ghosts.add(Game.blinky);
    Game.ghosts.add(Game.clyde);
    Game.ghosts.add(Game.pinky);
    Game.ghosts.add(Game.inky);
    Game.ghosts.forEach(Game.setAnimations,this);
    Game.ghosts.forEach(Game.shrinkBody,this); // Make body smaller to prevent game over when pacman barely touches a ghost; we want more overlap
    // With setAll, last parameter is true to force the setting of the value of a yet non-existing property
    Game.ghosts.setAll('speed',255,false,false,0,true); // It's actually the time to cross a cell, so the higher, the slower
    Game.ghosts.setAll('pelletThreshold',-1,false,false,0,true); // How many pellets need to be eaten before a ghost leaves the ghost house
    Game.ghosts.setAll('frozen',false,false,false,0,true);
    Game.ghosts.setAll('eaten',false,false,false,0,true); // Eaten by Pacman while in frozen mode
    Game.ghosts.setAll('currentDir','',false,false,0,true); // Direction faced by the ghost
    Game.ghosts.setAll('canTween',true,false,false,0,true);
    Game.ghosts.setAll('bnfTween',null,false,false,0,true); // Tween for back-and-forth (bnf) movement in the ghost house
    Game.ghosts.setAll('distance',999,false,false,0,true); // Distance to target
    Game.ghosts.setAll('currentAnimation',1,false,false,0,true);
    Game.ghosts.setAll('tween',null,false,false,0,true);
    Game.pinky.frame += 28;
    Game.inky.frame += 28;
    Game.backAndForth(Game.clyde,'x',Game.clyde.x+(3*32));
    Game.backAndForth(Game.inky,'x',Game.inky.x-(3*32));
    Game.clyde.pelletThreshold = 100;
    Game.inky.pelletThreshold = 30;
    Game.blinky.name = 'blinky';
    Game.pinky.name = 'pinky';
    Game.inky.name = 'inky';
    Game.clyde.name = 'clyde';

    Game.barrier = game.add.sprite(13*32,12*32,'barrier',0);
    game.physics.arcade.enable(Game.barrier);
    Game.barrier.body.immovable = true;
    Game.barrier.animations.add('buzz', [0, 1, 2], 10, true);
    Game.barrier.animations.play('buzz');

    game.camera.follow(Game.pacman);
    //game.time.advancedTiming = true;

    Game.cursors = game.input.keyboard.createCursorKeys();
};

Game.shrinkBody = function(sprite){
    sprite.body.setSize(6,6,13,13);
};

Game.setAnimations = function(sprite){
    sprite.animations.add('freeze', [12, 13], 10, true);
    sprite.animations.add('invisible', [40, 41], 10, true);
    sprite.animations.add('down', [sprite.frame+14, sprite.frame+15], 10, true);
    sprite.animations.add('up', [sprite.frame+42, sprite.frame+43], 10, true);
    sprite.animations.add('left', [sprite.frame+28, sprite.frame+29], 10, true);
    sprite.animations.add('right', [sprite.frame, sprite.frame+1], 10, true);
};

Game.eatPellet = function(_pacman,_pellet){
    _pellet.destroy();
    Game.eatenPellets++;
};

Game.eatSuperPellet = function(_pacman,_pellet){
    _pellet.destroy();
    Game.ghosts.forEach(Game.freeze,this);
    game.tweens.pauseAll();
    game.time.events.add(Phaser.Timer.SECOND * 5, Game.unFreezeAll, this);
};

Game.freeze = function(ghost){
    ghost.frozen = true;
    ghost.animations.play('freeze');
};

Game.unFreezeAll = function(){
    Game.ghosts.setAll('frozen',false);
    Game.ghosts.forEach(function(ghost){
        if(!ghost.eaten) {
            ghost.animations.play(ghost.currentDir);
        }
    },this);
    game.tweens.resumeAll();
};

Game.computeStraightLine = function(path,coord){
    for(var nbStraight = 1; nbStraight < path.length-1; nbStraight++){
        if(path[1+nbStraight][coord] != path[1][coord]){
            break;
        }
    }
    return nbStraight;
};

Game.findPath = function(agent,target,pursue=true){ // pursue: should the agent travel the path found
    if(agent.canTween == true ){ // && agent.x%32 == 0 && agent.y%32 == 0
        var start_x = Phaser.Math.snapToFloor(Math.floor(agent.x), 32) / 32;
        var start_y = Phaser.Math.snapToFloor(Math.floor(agent.y), 32) / 32;
        var end_x = Phaser.Math.snapToFloor(Math.floor(target.x), 32) / 32;
        var end_y = Phaser.Math.snapToFloor(Math.floor(target.y), 32) / 32;
        if(start_x == end_x && start_y == end_y){return;}
        Game.easystar.findPath(start_x, start_y, end_x, end_y, function (path) {
            agent.distance = path.length;
            if(!pursue){return;}
            var start_x = Phaser.Math.snapToFloor(Math.floor(agent.x), 32) / 32;
            var start_y = Phaser.Math.snapToFloor(Math.floor(agent.y), 32) / 32;
            var delta_x = path[1].x - start_x;
            var delta_y = path[1].y - start_y;
            var delta = (delta_x != 0 ? delta_x : delta_y);
            var start = (delta_x != 0 ? start_x : start_y);
            var coord = (delta_x != 0 ? 'x' : 'y');
            // To make smooth movements, we want one single tween as long as the ghost doesn't have to turn, so we need to compute
            // the distance over which the tween will take effect, and compute the duration of the tween accordingly
            var nbStraight = Game.computeStraightLine(path,(delta_x != 0 ? 'y' : 'x'));
            var dest = (start+(nbStraight*Math.sign(delta)))*32;
            Game.tweenAgent(agent, coord, dest,nbStraight);
            if(delta_x < 0){var dir = 'left';}
            else if(delta_x > 0){var dir = 'right';}
            else if(delta_y < 0){var dir = 'up';}
            else if(delta_y > 0){var dir = 'down';}
            Game.animateAgent(agent,dir);
        });
        Game.easystar.calculate();
    }
};

Game.tweenAgent = function(agent,coord,dest,nb){
    var tween = game.add.tween(agent);
    agent.canTween = false; // Prevent new tweens before the current one is over
    var to = (coord == 'x'? {x: dest} : {y: dest});
    tween.to(to, nb*agent.speed,null, false, 0);
    tween.onComplete.add(function () {
        agent.canTween = true;
    }, this);
    agent.tween = tween;
    tween.start();
};

Game.backAndForth = function(agent,coord,end){
    var tween = game.add.tween(agent);
    agent.bnfTween = tween;
    var to = (coord == 'x'? {x: end} : {y: end});
    var nb = Math.abs(end-agent[coord])/32;
    tween.to(to, nb*200,'Linear', true, 0,-1,true); // last true for yoyo effect, -1 for infinite loop
    var dir1, dir2;
    if(coord == 'x'){
        if(end > agent.x){
            dir1 = 'right';
            dir2 = 'left';
        }else{
            dir1 = 'left';
            dir2 = 'right';
        }
    }else if(coord == 'y'){
        if(end > agent.y){
            dir1 = 'down';
            dir2 = 'up';
        }else{
            dir1 = 'up';
            dir2 = 'down';
        }
    }

    Game.animateAgent(agent,dir1);
    agent.currentAnimation = 1;
    tween.onRepeat.add(function(){
        agent.currentAnimation *= -1;
        if(agent.currentAnimation == -1){
            Game.animateAgent(agent,dir2);
        }else{
            Game.animateAgent(agent,dir1);
        }
    });
};

Game.animateAgent = function(agent,dir){
    if(agent.eaten){
        agent.animations.play('invisible');
    }else{
        agent.animations.play(dir);
    }
    agent.currentDir = dir;
};

Game.cancelBnF = function(agent){
    if(agent.bnfTween && !agent.frozen) {
        agent.bnfTween.stop();
        agent.bnfTween = null;
    }
};

Game.inStopRange = function(agent,coord){
    var correction = agent.speed/60;
    // correction because it'll take one frame for the velocity to drop to 0, during which pacman will move by speed/fps
    return ((agent[coord]+correction)%32 == 0 || (agent[coord]-correction)%32 == 0);
};

Game.checkTargetCell = function(x,y){
    var x_cell = Phaser.Math.snapToFloor(Math.floor(x), 32) / 32;
    var y_cell = Phaser.Math.snapToFloor(Math.floor(y), 32) / 32;
    //Map is 27x31
    if(x_cell < 0 || x_cell >= Game.collisionArray[0].length){return false;}
    if(y_cell < 0 || y_cell >= Game.collisionArray.length){return false;}
    return (Game.collisionArray[y_cell][x_cell] == 0);
};

Game.computeFuturePos = function(futureSteps){
    var futureX = Game.pacman.x;
    var futureY = Game.pacman.y;
    if(Game.pacman.orientation == 'right')
    {
        futureX += futureSteps*32;
    }else if(Game.pacman.orientation == 'left'){
        futureX -= futureSteps*32;
    }else if(Game.pacman.orientation == 'up'){
        futureY -= futureSteps*32;
    }else if(Game.pacman.orientation == 'down'){
        futureY += futureSteps*32;
    }
    return [futureX,futureY];
};

Game.cellFormat = function(a){
    return Phaser.Math.snapToFloor(Math.floor(a), 32) / 32;
};

// If target is a cell that cannot be travelled to (because it collides), find nearest ok cell around it
Game.correctTarget = function(x,y){
    var factor = 1;
    var new_x = x;
    var new_y = y;
    while (!Game.checkTargetCell(x,y)) {
        for(x_offset = -1; x_offset <= 1; x_offset++){
            for(y_offset = -1; y_offset <= 1; y_offset++){
                new_x = x + x_offset*factor*32;
                new_y = y + y_offset*factor*32;
                if(Game.checkTargetCell(new_x,new_y)){return {x:new_x,y:new_y};}
            }
        }
        factor++;
    }
    return {x:new_x,y:new_y};
};

Game.contact = function(_pacman,_ghost){
    if(_ghost.eaten){
        return;
    }else if(_ghost.frozen){
        _ghost.frozen = false;
        _ghost.eaten = true;
        _ghost.tween.stop(true);
    }else{
        Game.over = true;
        Game.pacman.body.velocity.x = 0;
        Game.pacman.body.velocity.y = 0;
        Game.pacman.animations.stop(true, true);
        var gameover = game.add.bitmapText(game.world.centerX, game.world.centerY, 'gameover', 'GAME OVER', 64);
        gameover.anchor.setTo(0.5);
        game.tweens.pauseAll();
    }
};

Game.findTarget = function(ghost){
    var target = null;
    switch(ghost){
        case 'blinky':
            target = {x:Game.pacman.x,y:Game.pacman.y};
            break;
        case 'pinky':
            var futureSteps = 4; // Target = pacman position in 4 "steps"
            var futurePos = Game.computeFuturePos(futureSteps);
            target = {x:futurePos[0],y:futurePos[1]};
            break;
        case 'inky':
            var futureSteps = 2;
            futurePos = Game.computeFuturePos(futureSteps);
            var delta_x = futurePos[0] - Game.blinky.x;
            var delta_y = futurePos[1] - Game.blinky.y;
            var end_x = Game.blinky.x + 2*delta_x;
            var end_y = Game.blinky.y + 2*delta_y;
            target = {x:end_x,y:end_y};
            break;
        case 'clyde':
            Game.findPath(Game.clyde, Game.pacman, false);
            target = (Game.clyde.distance < 8 ? {x: 1 * 32, y: 29 * 32} : {x:Game.pacman.x,y:Game.pacman.y});
    }
    return Game.correctTarget(target.x,target.y);
};

Game.update = function(){
    game.physics.arcade.overlap(Game.pacman, Game.ghosts,Game.contact);
    if(Game.over){return;}
    game.physics.arcade.collide(Game.pacman, Game.layer);
    game.physics.arcade.collide(Game.pacman, Game.barrier);
    game.physics.arcade.overlap(Game.pacman, Game.pellets,Game.eatPellet);
    game.physics.arcade.overlap(Game.pacman, Game.superPellets,Game.eatSuperPellet);
    if(Game.cursors.left.isDown)
    {
        Game.pacman.body.velocity.x = -Game.pacman.speed;
        Game.pacman.orientation = 'left';
        Game.pacman.animations.play('left');
    }
    else if(Game.cursors.right.isDown)
    {
        Game.pacman.body.velocity.x = Game.pacman.speed;
        Game.pacman.orientation = 'right';
        Game.pacman.animations.play('right');
    }else if(Game.inStopRange(Game.pacman,'x')){ // If within the next frame Pacman will reach a cell, then you can stop, otherwise keep going until his position exactly matches a cell
        Game.pacman.body.velocity.x = 0;
    }else if(Game.cursors.up.isDown)
    {
        Game.pacman.body.velocity.y = -Game.pacman.speed;
        Game.pacman.orientation = 'up';
        Game.pacman.animations.play('up');
    }
    else if(Game.cursors.down.isDown)
    {
        Game.pacman.body.velocity.y = Game.pacman.speed;
        Game.pacman.orientation = 'down';
        Game.pacman.animations.play('down');
    }else if(Game.inStopRange(Game.pacman,'y')){
        Game.pacman.body.velocity.y = 0;
    }

    if(Game.pacman.body.velocity.x == 0 && Game.pacman.body.velocity.y == 0){
        Game.pacman.animations.stop(true,true); // true to reset to first frame of animation
    }

    game.world.wrap(Game.pacman, 0, true);

    Game.ghosts.forEach(function(ghost){
        if (Game.eatenPellets >= ghost.pelletThreshold && !ghost.frozen) {
            Game.cancelBnF(ghost);
            var target = (ghost.eaten ? {x:13*32,y:14*32} : Game.findTarget(ghost.name,ghost.eaten));
            if(ghost.eaten && target.x == ghost.x && target.y == ghost.y){ghost.eaten = false;}
            Game.findPath(ghost, target);
        }
    },this);

    /*var target = Game.findPinkyTarget();
    console.log(Game.cellFormat(target.x)+' '+Game.cellFormat(target.y)+' '+Game.collisionArray[Game.cellFormat(target.y)][Game.cellFormat(target.x)]);
    Game.pinky.x = target.x;
    Game.pinky.y = target.y;*/
};

Game.shutdown = function(){

};

Game.render = function(){
    //game.debug.body(Game.pellets);
    //game.debug.text(game.time.fps, 2, 14, "#00ff00");
};